# 2x28BYJ-48 Pan-Tilt (PT) mount for TOF400C

- [ti:ULN2803C](https://www.ti.com/product/ULN2803C)
- XH JST 800921 (5P)

## Presupuesto

13.75€

- 1x TOF400C 4€ = 4€
- 2x (28BYJ-48 + ULN2003) 2.25€ = 4.5€
- 1x Arduino Nano 2.5€ = 2.5€
- 1x Rotary Encoder + PushButton 1€ = 1€
- 1x Laser 650nm 0.75€ = 0.75€
- Cables 1€ = 1€
