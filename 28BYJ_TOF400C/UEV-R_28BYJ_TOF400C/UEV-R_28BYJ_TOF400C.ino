#include <Stepper.h>

const int stepsPerRevolution = 2048;

// IN1 IN3 IN4 IN2
// Blue, Yellow, Orange, Pink
Stepper myStepperR(stepsPerRevolution, 4, 6, 7, 5);
Stepper myStepperI(stepsPerRevolution, 8, 10, 11, 9);

void setup() {
  myStepperR.setSpeed(12);
  myStepperI.setSpeed(12);
}

void loop() {
  myStepperR.step(stepsPerRevolution);
  delay(1000);
  myStepperI.step(stepsPerRevolution);
  delay(1000);
}
