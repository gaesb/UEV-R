# UEV-R

Prototipo de un escaner láser 3D motorizado y automatizado para la captura de nubes de puntos en cavidades:

- Alcance, precisión y exactitud similares al [DistoX2](https://gaesb.gitlab.io/material/topo/hw/distox.html).
- Rango de rotación de 360º e inclinación de al menos [-45º,90º].
- Conectividad Bluetooth.
- Compacto, robusto y fiable en entornos con barro y arena.
- Autonomía de varias horas.
- Coste preferiblemente inferior a 150€ y no superior a 300€.
- Fuentes abiertas (*open source*).

## Hardware

![](img/UEV-R.png)

### Distanciómetro digital

En el grupo disponemos de un Disto A3 (que no se puede modificar a DistoX porque se estropeó al intentarlo), además del
DistoX2 (x310) que utilizamos.
Lamentablemente, no resulta fácil prototipar un escaner 3D utilizando los Disto porque el firmware no es abierto, por lo
que habría que descubrir cómo interactuar con el sensor de forma automatizada (sin requerir la pulsación).
Si en más de una década de trayectoria del DistoX sólo parece haber una persona que haya modificado el firmware, no
parece muy factible.

Asimismo, parece ser que no hay sensores láser con alcance mayor a 4-5 m pero menor que 40 m.
Lo cual es desafortunado porque los de 40 m cuestan prácticamente lo mismo que los 100 m, 140 m, 150 m,...
Por lo tanto, en caso de comprar uno "para hacer pruebas" merecería la pena comprar el definitivo.

Si dispusiéramos de un SAP, podríamos cambiar el firmware temporalmente para utilizar sólo el medidor de distancia y
hacer pruebas de captura de nubes de puntos.
Puesto que el sensor solo cuesta más de la mitad de lo que cuesta el SAP completo, y ya que el Disto x310 está
descatalogado con un SAP podríamos "matar dos pájaros de un tiro" (probar un reemplazo para el DistoX2 y prototipar
UEV-R).

`laser meter TTL`

- Egismos Laser Distance Module - Model 2 (LDK-2M-100-RS) [$150.00]
  - 100 m ± 3.0mm @ 1-10 Hz
  - 37.5 x 45.3 x 19.2 mm
  - UART @ 9600
  - 2.5-3.0 V
- Jedrek L150 [100€]
  - 140 m ± 1.0mm @ 1-20 Hz
  - 48 x 38 x 18 mm
  - UART
  - 3-5V
- DFRobot SEN0366 [80€]
  - 80 m ± 1.0mm @ 1-20 Hz
  - 48 x 42 x 18 mm
  - UART @ 9600
  - 3.3-5V
- JRT BA6A|B87A
  - 150 m ± 3.0mm @ 3 Hz
  - 64 x 40 x 18 mm
  - UART @ 19200
  - 3-3.3V

```{note}
En la página de JRT, en el FAQ de algunos productos se indica:

*Q: Can I have a sample for testing?*
*A: Yes. We do not offer free samples, but will reimburse the buyer once an order is confirmed.*

Por lo que una opción para el prototipado puede ser pedir una muestra a JRT.
```

### Diseño mecánico

`pan tilt stepper`

Se requieren dos ejes de rotación, denominados *pan* y *tilt* en ingles.

El [Leica 3D Disto (HS)](https://www.disto.es//leica-disto.php?REF=844692) (ver también [Leica 3D Disto](https://www.disto.es/disto_3d/leica_disto_3D.php)),
combina un medidor de distancia con dos ejes motorizados para controlar la rotación y la inclinación.
Sin embargo, se usa mediante software en una tableta, ya que está pensado para su uso en arquitectura y diseño de
interiores.
El precio del producto en su conjunto es de varios miles de euros, y no resultaría suficiente para topografía
espeleológica porque el alcance es de 50 m.

El término *pan-tilt-zoom* (PTZ) es habitual en el ámbito de las cámaras de video(vigilancia):
[w:Pan–tilt–zoom camera](https://en.wikipedia.org/wiki/Pan%E2%80%93tilt%E2%80%93zoom_camera).
Sin embargo, dado que las cámaras tienen una campo vertical notablemente mayor que el de un distanciómetro, el rango de
inclinación suele ser reducido (<90º).
Además, son voluminosos (ya que las cámaras de fotos o vídeo tienen mayor tamaño que un láser) y se controlan mediante
movimientos predefinidos, un mando a distancia específico, o software específico.
Por ejemplo, los modelos Zifon YT-800 e YT-1200 disponibles por 50-55€ en AliExpress tienen un rango de inclinación de
50º y 35º, respectivamente; y se controlan mediante un mando específico:

![](img/YT-800.png)

![](img/YT-1200.png)

Para su uso en espeleología no necesitamos control remoto, sino poder programar secuencias específicas a modo de
cuadrícula/malla.
Tomando como referencia la revolución de las impresoras 3D de bajo coste de la última década, podemos utilizar motores
paso a paso.

En el campo de la fotografía y la creación de contenido es habitual añadir la traslación lineal (*slide*) a los ejes
*pan* y *tilt*.
Hay varios diseños que utilizan motores y electrónica similares a las impresoras 3D:

- [gh: isaac879/Pan-Tilt-Mount](https://github.com/isaac879/Pan-Tilt-Mount)
  - [thing:4316563](https://www.thingiverse.com/thing:4316563)
  - [yt:uJO7mv4-0PY](https://www.youtube.com/watch?v=uJO7mv4-0PY)
  - [yt:v1b7Wvu87-U](https://www.youtube.com/watch?v=v1b7Wvu87-U)
- [yt:1y01CJpBQ1U](https://www.youtube.com/watch?v=1y01CJpBQ1U)
- [yt:IBZoRFJd7Cw](https://www.youtube.com/watch?v=IBZoRFJd7Cw)

Sin embargo, uno de los retos principales de este dispositivo es el consumo de energía.
Los motores consumen mucho más que la electrónica, y la estimación es que se necesitan varias horas continuadas de uso
si se quieren escanear media docena de salas/galerías en una sesión.
Por ello, los diseños basados en NEMA17 o NEMA14 son en principio demasiado voluminosos y potentes para nuestras
necesidades.

El sistema de movimiento de rotación e inclinación debe minimizar el rozamiento para así minimizar el requerimiento de
par de los motores, sin que ello transfiera excesivo peso al eje (lo que produciría holguras a la larga).

```{note}
Puede ser interesante echar un vistazo a los productos disponibles para astronomía.
Para compensar la rotación de la tierra, o hacer seguimiento de ciertos objetos celestes, se venden kits de dos motores
para montar entre el trípode y el telescopio a partir de 150-200€.
Las diferencias principales son que los telescopios pesan más que el medidor de distancia que nosotros necesitamos, y
que la velocidad de rotación/traslación de la Tierra o los objetos celestes es mucho más lenta que lo que requerimos
para capturar nubes de puntos.
Por otro lado, los kits parecen estar pensados para su uso con baterías, por lo que podemos tomar referencias en cuanto
a potencia y autonomía.
```

## Software

### Prototipo

Se puede trabajar en el firmware y el software con hardware de juguete, antes de escalarlo.

En el ecosistema Arduino son comunes los servos SG90 y los sensores de distancia por ultrasonido HC-SR04.
Los SG90 son servos y no paso a paso, y pueden rotar aproximadamente 180º (90º en cada sentido).
El rango de medida del sensor de distancia es de 2-400 cm y una precisión de hasta 3 mm (que en la práctica son 3-4 cm).
Aunque son insuficientes para la aplicación final, sirven para escanear medias estancias, y probar las herramientas
software para ver y procesar la nube de puntos.

Ver [SG90_HCSR04/](SG90_HCSR04/).

Una vez realizadas las primeras pruebas con dos SG90 y un HC-SR04, se valida el concepto ya que se consigue obtener
medidas siguiendo un patrón de malla/cuadrícula, transferirlas al ordenador, crear un fichero PCD y visualizarlo
utlizando varias herramientas (Matlab y threejs).
Sin embargo, se aprecian las siguientes limitaciones:

- El sistema de control de los servos provoca movimientos muy bruscos, particularmente al arrancar, ya que se dirigen
  muy rápidamente a la posición indicada por el control.
  Puesto que se trata de un control en lazo abierto, no es posible saber cuál es la ubicación de origen (lo que
  permitiría suavizar las transiciones).
- El soporte pan-tilt impreso con impresora 3D tiene holguras que junto con la brusquedad de los motores hacen muy
  impreciso el posicionamiento.
    - Esto puede resolverse dedicando mayor tiempo y esfuerzo a ajustar los modelos y la impresora 3D, pero no merece
      la pena.
      Por poco más de un euro podemos adquirir en Aliexpress ([aliexpress.com/item/32717741094](https://es.aliexpress.com/item/32717741094.html))
      una versión fabricada con molde, que debería tener un ajuste mejor.
- Debido al poco peso de la base, a que el cable del servo de rotación impide que apoye bien, a la rigidez de los
  propios cables y los movimientos bruscos, la base se mueve rotando sobre si misma.
    - Esto puede resolverse atornillando la base o añadiendo peso a la misma, y sujetando los cables a un punto fijo.
      Sin embargo, no merece la pena hacerlo hasta no resolver el punto anterior.
- Como el sensor HC-SR04 funciona por ultrasonido, no se ve a qué punto está apuntando ni cuál es la anchura del haz.
    - Puede mitigarse parcialmente utilizando un puntero y alineándolo aproximadamente.
      No obstante, no merece la pena hacerlo hasta no resolver los puntos anteriores.
- Por el principio de funcionamiento, el sensor por ultrasonido tiene un ángulo de lectura de 15º, que es bastante amplio.
  Por lo que a aproximadamente 2 m de distance, no puede diferenciar detalles de menos de medio metro.
  Para detectar objetos de aproxidamente 10 cm, deben estar a no más de 40 cm del sensor.

Aunque la mayoría de los puntos anteriores pueden paliarse, al menos parcialmente, resulta razonable plantear la
elaboración de un segundo prototipo utilizando motores paso a paso y un sensor láser.
Mientras tanto, podemos mejorar el primer prototipo para ir elaborando el firmware que permita mover manualmente ambos
ejes para apuntar a dos puntos conocidos antes de empezar el escaneo.
Esas mejoras del firmware serán transferibles al segundo prototipo.

Los motores paso a paso más simples y comunes en el ecosistema Arduino son los 28BYJ-48 de cuatro pasos por vuelta, con
una reductora 1/64 y que se pueden alimentar con 5-12 V.
Controlados mediante medios pasos, son 512 medios pasos por vuelta, $0,7º$ por medio paso.
Suelen venderse junto con ULN2003 como driver/etapa de potencia.
Asimismo el sensor VL53L1X se vende como TOF400C listo para ser alimentado con 3-5 V y usado mediante interfaz I2C.

Ver [28BYG_TOF400C/](28BYG_TOF400C/).

`stepper gimbal 2 axis`

- [thing:5461912](https://www.thingiverse.com/thing:5461912)
- [yt:_yVqxHHbBU8](https://www.youtube.com/watch?v=_yVqxHHbBU8)
  - [grabcad.com/library/gimbal-65](https://grabcad.com/library/gimbal-65)
- [yt:qewloVWAygA](https://www.youtube.com/watch?v=qewloVWAygA)
  - [tinkercad.com/things/kkIE3qFwVO1-28byj-48-gimbal-base](https://www.tinkercad.com/things/kkIE3qFwVO1-28byj-48-gimbal-base)
  - [yt:jZeEUL3uws4](https://www.youtube.com/watch?v=jZeEUL3uws4)
- [thing:4561012](https://www.thingiverse.com/thing:4561012)
- [thing:794786](https://www.thingiverse.com/thing:794786)

### Firmware

Con un paso de 1,8º tendríamos `(360º/1,8º)^2 = 40.000` puntos.

En la práctica, a 1 segundo por captura, se necesitarían `40000/3600 = 11,11h`.
Por lo tanto, una medida más razonable es tomar 3600 puntos en 1 h, lo que son 60 puntos por dimensión en pasos de 6º.
O, en múltiplos de 1,8º, serían pasos de 7,2º para un total de 50 pasos por dimensión, 2500 puntos en total en 42 minutos.

¿Debería modelarse la cuadrícula/malla como una esfera o un prisma?

### 3D

- https://therion.speleo.sk/wiki/examples#export_to_3d_data_clouds_processing_softwares
  - CloudCompare
  - ParaView
- https://threejs.org/docs/#api/en/math/Vector3.setFromSphericalCoords

#### PCD

- https://github.com/nandini-menon/Point-Cloud-Generation
- https://threejs.org/docs/#examples/en/loaders/PCDLoader
  - https://github.com/mrdoob/three.js/blob/master/examples/webgl_loader_pcd.html
- https://pointclouds.org/documentation/tutorials/pcd_file_format.html
- https://www.mathworks.com/help/vision/ref/pcread.html

## Procedimiento

Nótese que no es necesario que el medidor disponga de compás ni clisímetro, ya que los motores paso a paso
producen un posicionamiento preciso.
El procediemiento sería el siguiente:

- Situar el trípode en el centro de la sala/galería que se desee escanear.
- Con el control manual (sea a través de un dispositivo con Bluetooth, o con un encoder rotatorio y un pulsador), apuntar
  a uno o varios vértices de la poligonal, que definirán las referencias del sistema de coordenadas de la esfera.
- Iniciar el escaneo y salir de la sala/galería hasta que termine.
- Retirar el trípode.

## LIDAR

- [yt:RrjU4P3t2HE: 3D cave mapping with the BLK2GO](https://www.youtube.com/watch?v=RrjU4P3t2HE)
- [yt:wcK2o3aKlGc: BLK 360 - First test in cave](https://www.youtube.com/watch?v=wcK2o3aKlGc)
  - [shop.leica-geosystems.com/leica-blk/blk360/blog/video/blk360-first-test-cave](https://shop.leica-geosystems.com/leica-blk/blk360/blog/video/blk360-first-test-cave)
- [yt:yxVWNjO12Og: RIEGL VZ-400i: Underground 3D Laser Scanning in Caves - RELOADED](https://www.youtube.com/watch?v=yxVWNjO12Og)
- [geoslam.com](https://geoslam.com)
  - [ZEB Go](https://geoslam.com/solutions/zeb-go/)
  - [ZEB Revo RT](https://geoslam.com/solutions/zeb-revo-rt/)
  - [ZEB Horizon](https://geoslam.com/solutions/zeb-horizon/)
    - [ZEB Horizon RT](https://geoslam.com/solutions/zeb-horizon-rt/)
- [gh:snt-arg/lidar_situational_graphs](https://github.com/snt-arg/lidar_situational_graphs)

- [caveatron.com](https://www.caveatron.com/)
  - Iñaki Alonso 'Indio' de Akerbeltz (Tolosa), construyó un Caveatron.
    Lamentablemente, falleció en enero de 2022, y la página del grupo parece estar caída
    ([akerbeltzespeleo.com/caveatron-results/](https://akerbeltzespeleo.com/caveatron-results/)).

## References

- [dustinpfister.github.io/2022/02/04/threejs-vector3-set-from-spherical-coords](https://dustinpfister.github.io/2022/02/04/threejs-vector3-set-from-spherical-coords/)
