# 2xSG90 Pan-Tilt (PT) mount for HC-SR04

- [thing:2467743](https://www.thingiverse.com/thing:2467743)
- `arm_short.stl`, `high_head_with_hounds.stl` and `hcsr04_support.stl` modified by Unai Martinez-Corral.

## Presupuesto

9€

- 1x HC-SR04 2€ = 2€
- 2x SG90 1€ = 2€
- 1x Pan-Tilt 1.5€ = 1.5€
- 1x Arduino Nano 2.5€ = 2.5€
- Cables 1€ = 1€
