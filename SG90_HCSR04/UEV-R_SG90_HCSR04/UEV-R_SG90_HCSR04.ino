#include <Servo.h>

Servo rotation;
Servo inclination;

int idx, pos_rot, pos_incl = 0;

const int rot_min = 0;
const int rot_max = 135;
const int rot_delay = 250;

const int incl_min = 5;
const int incl_max = 95;
const int incl_delay = 250;

const int rot_step = 5;
const int incl_step = 5;

const int Trigger = 3;
const int Echo = 2; 

long t;

void setup() {
  Serial.begin(9600);
  pinMode(Trigger, OUTPUT);
  pinMode(Echo, INPUT);
  digitalWrite(Trigger, LOW);
  rotation.attach(6, 750, 2350);
  inclination.attach(5, 550, 2400);

  rotation.write(rot_min);
  inclination.write(incl_min);
  delay(max(rot_delay,incl_delay));
}

void get_distance() {
  digitalWrite(Trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(Trigger, LOW);
  t = pulseIn(Echo, HIGH);
  char buffer[40];
  // R 000.00
  // I 000.00
  // D 000.00
  sprintf(buffer, "M%04d R%05d I%05d D%05d", idx++, pos_rot*100, pos_incl*100, int(t/59));
  Serial.println(buffer);
}

void loop() {
  while (Serial.available() <= 0) {
    Serial.println("Waiting for init. Send anything!");
    delay(5000);
  }
  String inputString = "";
  while (Serial.available() > 0) {
    char inChar = (char)Serial.read();
    if (inChar == '\n') {
      Serial.println(inputString);
      switch(inputString[0]) {
        case 'R':
          Serial.println("Rotation command!");
          inputString.remove(0, 1);
          rotation.write(inputString.toInt());
          delay(rot_delay);
          break;
        case 'I':
          Serial.println("Inclination command!");
          inputString.remove(0, 1);
          inclination.write(inputString.toInt());
          delay(incl_delay);
          break;
        case 'S':
          Serial.println("Scan command!");
          scan();
          break;
        default:
          Serial.println("Unknown command!");
      }
    } else {
      inputString += inChar;
    }
  }
}

void scan() {
  idx = 0;
  rotation.write(rot_min);
  inclination.write(incl_min);
  delay(max(rot_delay,incl_delay));

  for (pos_incl = incl_min; pos_incl <= incl_max; pos_incl += incl_step) {
    inclination.write(pos_incl);
    delay(incl_delay);
    for (pos_rot = rot_min; pos_rot <= rot_max; pos_rot += rot_step) {
      rotation.write(pos_rot);
      delay(rot_delay);
      get_distance();
    }
    pos_incl += incl_step;
    inclination.write(pos_incl);
    delay(incl_delay);  
    for (pos_rot = rot_max; pos_rot >= rot_min; pos_rot -= rot_step) {
      rotation.write(pos_rot);
      delay(rot_delay);
      get_distance();
    }
  }
  for (pos_incl = incl_max; pos_incl >= incl_min; pos_incl -= incl_step) {
    pos_incl -= incl_step;
    inclination.write(pos_incl);
    delay(incl_delay);  
  }
}
