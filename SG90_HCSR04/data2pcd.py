#!/usr/bin/env python3

from pathlib import Path

import numpy


with open(Path('data.dat'), 'r', encoding='utf-8') as rdptr:
  data = rdptr.read().splitlines()

xyz = []

for item in data:
  [Rotation, Inclination, Distance] = item.split()
  Rotation = numpy.deg2rad(float(Rotation[1:-1]))
  Inclination = numpy.deg2rad(float(Inclination[1:-1]))
  Distance = float(Distance[1:-1])

  # From spherical coordinate system to cartesian coordinate system
  x = Distance * numpy.sin(Inclination) * numpy.cos(Rotation)
  y = Distance * numpy.sin(Inclination) * numpy.sin(Rotation)
  z = Distance * numpy.cos(Inclination)

  xyz.append(f"{x} {y} {z}")

xyz = '\n'.join(xyz)

with open(Path('data.pcd'), 'w', encoding='utf-8') as wrptr:
  wrptr.write(f'''
# .PCD v.7 - Point Cloud Data file format
VERSION .7
FIELDS x y z
SIZE 4 4 4
TYPE F F F
COUNT 1 1 1
WIDTH {len(xyz)}
HEIGHT 1
VIEWPOINT 0 0 0 1 0 0 0
POINTS 250
DATA ascii
{xyz}
''')
