#!/usr/bin/env python3

from pathlib import Path

import serial


raw = []
archive = []

#port = 'COM4'
port = '/dev/ttyACM0'

ser = serial.Serial(port, 9600, timeout=1)
state = "Waiting"
while state != "Scan":
  got = ser.readline().decode().strip()
  if len(got) > 0:
    print("[Waiting for device to be ready for a new scan]", got)
    if state in got:
      state = "Scan"
      ser.write(b'S\n')

while state != "Done":
  got = ser.readline().decode().strip()
  if len(got) > 0:
    if "Waiting" in got:
        state = "Done"
    elif got[0] in ["S", "R", "I"]:
        continue
    else:
      print(got, end="")
      raw.append(got)
      [Type, Rotation, Inclination, Distance] = got.split()
      if Type[0] not in ['V', 'M']:
        raise TypeError(f"Format error! Type: {Type}")
      if Rotation[0] != 'R':
        raise TypeError(f"Format error! Rotation: {Rotation}")
      if Inclination[0] != 'I':
        raise TypeError(f"Format error! Inclination: {Inclination}")
      if Distance[0] != 'D':
        raise TypeError(f"Format error! Distance: {Distance}")

      # V|M 0000
      # R 000.00 º
      # I 000.00 º
      # D 000.00 m
      dat = f"|{float(int(Rotation[1:]))/100:.2f}º /{float(int(Inclination[1:]))/100:.2f}º _{float(int(Distance[1:]))/100:.2f}m"
      archive.append(dat)
      print(f" {dat}")

with open(Path('raw.dat'), 'w', encoding="utf-8") as wrptr:
  wrptr.write('\n'.join(raw))

with open(Path('data.dat'), 'w', encoding="utf-8") as wrptr:
  wrptr.write('\n'.join(archive))
